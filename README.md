# @bsara/eslint-config-react [![NPM Package](https://img.shields.io/npm/v/@bsara/eslint-config-react.svg?style=flat-square)][npm]


[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)][license]


[ESLint][eslint] shareable configs for JavaScript coding standards created by
[bsara][bsara-home] for projects using React.


[Changelog](https://github.com/bsara/eslint-config-bsara-react/blob/master/CHANGELOG.md)




# Install

**Project Install**
```bash
$ npm install --save-dev eslint @bsara/eslint-config-react
```

**Global Install**
```bash
$ npm install -g eslint @bsara/eslint-config-react
```




# Usage

Create an [ESLint configuration file][config-file-formats] at the root of your project
called `.eslintrc.yml` or `.eslintrc.json` and add the following to file:

```json
{
  "extends": "@bsara/react"
}
```

Your local configuration will inherit all of the settings/rules defined by
`@bsara/eslint-config-react`. Any settings/rules found in your local configuration file
that conflict with a setting/rule found in `@bsara/eslint-config-react` will override
whatever is found in `@bsara/eslint-config-react`.

See the official ESLint documentation for more information about [Shareable Configs][shareable-configs].

<br/>

##### [Sample YAML Configuration](https://github.com/bsara/eslint-config-bsara-react/blob/master/sample.eslintrc.yml)

```yaml
root: true

parserOptions:
  sourceType: module

extends: @bsara/react

env:
  browser: true
  node:    true
```

<br/>

##### [Sample JSON Configuration](https://github.com/bsara/eslint-config-bsara-react/blob/master/sample.eslintrc.json)

```json
{
  "root": true,

  "parserOptions": {
    "sourceType": "module"
  },

  "extends": "@bsara/react",

  "env": {
    "browser": true,
    "node":    true
  }
}
```



# License

ISC License (ISC)

Copyright (c) 2017, Brandon D. Sara (http://bsara.pro/)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[bsara-home]: http://bsara.pro/
[license]:    https://github.com/bsara/eslint-config-bsara-react/blob/master/LICENSE "License"
[npm]:        https://www.npmjs.com/package/@bsara/eslint-config-react               "NPM Package: @bsara/eslint-config-react"

[config-file-formats]: http://eslint.org/docs/user-guide/configuring#configuration-file-formats "Configuration File Formats"
[eslint]:              http://eslint.org/                                                       "ESLint Home"
[shareable-configs]:   http://eslint.org/docs/developer-guide/shareable-configs                 "Shareable Configs"

## v1.2.0

#### New Rules

- [`react/no-typos`][rule-react-no-typos] = `'error'`
- [`react/no-unused-state`][rule-react-no-unused-state] = `'warn'`

#### Misc

- Updated to latest version of `eslint-plugin-react` (v7.2.0).
- Updated to latest version of [@bsara/eslint-config][base-npm] (v2.3.0) *(which contains rules for `eslint-plugin-import`)*.



## [v1.1.0](https://github.com/bsara/eslint-config-bsara/tree/v1.1.0) - 2017-08-08

#### Updated Rules

- [`filenames/match-regex`][rule-filenames-match-regex]: Added `<valid-js-file-name>.story.js` and `<valid-js-file-name>.spec.js` as valid file names.
- [`react/display-name`][rule-react-display-name]: Changed severity to warning.

#### Disabled Rules

- [`react/jsx-handler-names`][rule-react-jsx-handler-names]
- [`react/jsx-indent-props`][rule-react-jsx-indent-props] *(Only disabling until a "first" option is made available)*

#### Misc

- Updated to latest version of ESLint (v4.4.1).
- Updated to latest version of [@bsara/eslint-config][base-npm].



## [v1.0.5](https://github.com/bsara/eslint-config-bsara/tree/v1.0.5) - 2017-07-26

#### Misc

- Updated to latest version of [@bsara/eslint-config][base-npm].



## [v1.0.4](https://github.com/bsara/eslint-config-bsara/tree/v1.0.4) - 2017-07-21

#### Updated Rules

- [`react/props-types`][rule-react-prop-types]: Added ignored properties ("id", "children", "className", "style").


#### Fixes

- Fixed invalid option configurations introduced in last release
- Fixed some misspelled rule references.


#### Misc

- Updated to latest version of [@bsara/eslint-config][base-npm].
- Updated to ESLint version 4.3.0.



## [v1.0.0](https://github.com/bsara/eslint-config-bsara/tree/v1.0.0) - 2017-07-20

- Initial stable release





[base-npm]: https://www.npmjs.com/package/@bsara/eslint-config "NPM Package: @bsara/eslint-config"

[rule-filenames-match-regex]: https://github.com/selaux/eslint-plugin-filenames#consistent-filenames-via-regex-match-regex "ESLint Rule: filesnames/match-regex"

[rule-react-display-name]:      https://github.com/yannickcr/eslint-plugin-react/blob/HEAD/docs/rules/display-name.md      "ESLint React Rule: react/display-name"
[rule-react-jsx-handler-names]: https://github.com/yannickcr/eslint-plugin-react/blob/HEAD/docs/rules/jsx-handler-names.md "ESLint React Rule: react/jsx-handler-names"
[rule-react-jsx-indent-props]:  https://github.com/yannickcr/eslint-plugin-react/blob/HEAD/docs/rules/jsx-indent-props.md  "ESLint React Rule: react/jsx-indent-props"
[rule-react-no-typos]:          https://github.com/yannickcr/eslint-plugin-react/blob/HEAD/docs/rules/no-typos.md          "ESLint React Rule: react/no-typos"
[rule-react-unused-state]:      https://github.com/yannickcr/eslint-plugin-react/blob/HEAD/docs/rules/no-unused-state.md   "ESLint React Rule: react/no-unused-state"
[rule-react-prop-types]:        https://github.com/yannickcr/eslint-plugin-react/blob/HEAD/docs/rules/prop-types.md        "ESLint React Rule: react/prop-types"

/**
 * @bsara/eslint-config-react v1.2.0-beta.3
 *
 * ISC License (ISC)
 *
 * Copyright (c) 2017, Brandon D. Sara (http://bsara.pro/)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
module.exports = {

  extends: "@bsara",


  plugins: [
    'react'
  ],


  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },


  rules: {

    // Plugin: eslint-plugin-filenames
    // ----------------------------------------------------------

    // Errors
    'filenames/match-regex': [ 'error', '^((_{1,2}|\\.|[a-z\\$])([a-z0-9\\$]|[a-z0-9\\$]{1}\\-[a-z0-9\\$]{1}|[a-z0-9\\$]{1}\\.[a-z0-9\\$]{1})*?|[A-Z][a-zA-Z0-9]*|[A-Z][a-zA-Z0-9]+\\.story|[A-Z][a-zA-Z0-9]+\\.spec)$' ],



    // Plugin: eslint-plugin-react (Deprecations)
    // ----------------------------------------------------------

    // Errors
    'react/no-deprecated':          'error',
    'react/no-find-dom-node':       'error',
    'react/no-is-mounted':          'error',
    'react/no-render-return-value': 'error',
    'react/no-string-refs':         'error',



    // Plugin: eslint-plugin-react (Possible Errors)
    // ----------------------------------------------------------

    // Errors
    'react/default-props-match-prop-types': 'error',
    'react/jsx-key':                        'error',
    'react/jsx-no-comment-textnodes':       'error',
    'react/jsx-no-duplicate-props':         [ 'error', { ignoreCase: true } ],
    'react/jsx-no-undef':                   [ 'error', { allowGlobals: true } ],
    'react/no-typos':                       'error',
    'react/no-unescaped-entities':          'error',
    'react/react-in-jsx-scope':             'error',
    'react/require-render-return':          'error',
    'react/style-prop-object':              'error',
    'react/void-dom-elements-no-children':  'error',

    // Warnings
    'react/no-unused-prop-types': 'warn',
    'react/no-unused-state':      'warn',
    'react/prop-types':           [ 'warn', { ignore: [ 'id', 'children', 'className', 'style' ],  skipUndeclared: true } ],



    // Plugin: eslint-plugin-react (Best Security Practices)
    // ----------------------------------------------------------

    // Error
    'react/jsx-no-target-blank': 'error',



    // Plugin: eslint-plugin-react (Best Practices)
    // ----------------------------------------------------------

    // Error
    'react/no-children-prop':                     'error',
    'react/no-danger':                            'error',
    'react/no-danger-with-children':              'error',
    'react/no-direct-mutation-state':             'error',
    'react/no-redundant-should-component-update': 'error',
    'react/no-unknown-property':                  'error',

    // Warnings
    'react/display-name':              'warn',
    'react/forbid-foreign-prop-types': 'warn',
    'react/forbid-prop-types':         [ 'warn', { forbid: [ 'any' ] } ],
    'react/jsx-no-bind':               'warn',
    'react/no-array-index-key':        'warn',
    'react/no-did-mount-set-state':    [ 'warn', 'disallow-in-func' ],
    'react/no-did-update-set-state':   [ 'warn', 'disallow-in-func' ],
    'react/no-will-update-set-state':  [ 'warn', 'disallow-in-func' ],
    'react/prefer-stateless-function': 'warn',
    'react/require-optimization':      'warn',



    // Plugin: eslint-plugin-react (Stylistic Issues)
    // ----------------------------------------------------------

    // Errors
    'react/jsx-closing-bracket-location': [ 'error', { selfClosing: 'after-props', nonEmpty: 'after-props' } ],
    'react/jsx-closing-tag-location':     'error',
    // 'react/jsx-curly-spacing':             [ 'error', 'always', {  } ],
    'react/jsx-equals-spacing':           'error',
    'react/jsx-filename-extension':       [ 'error', { extensions: [ '.js', '.jsx' ] } ],
    'react/jsx-first-prop-new-line':      [ 'error', 'never' ],
    'react/jsx-indent':                   [ 'error', 2 ],
    'react/jsx-pascal-case':              'error',
    'react/jsx-tag-spacing':              'error',
    'react/jsx-wrap-multilines':          'error',
    'react/no-multi-comp':                [  'error', { ignoreStateless: true } ],

    // Warnings
    'react/prefer-es6-class':  [ 'warn', 'always' ],
    'react/self-closing-comp': 'warn',

    // Disabled
    'react/boolean-prop-naming': 'off',
    'react/jsx-handler-names':   'off',
    'react/jsx-indent-props':    'off', // [ 'error', 2 ], // TODO: Add back once 'first' option is available
    'react/jsx-no-literals':     'off',
    'react/sort-comp':           'off',



    // Plugin: eslint-plugin-react (Misc)
    // ----------------------------------------------------------

    // Errors
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars':  'error'
  }
};
